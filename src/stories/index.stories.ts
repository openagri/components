import { storiesOf, moduleMetadata } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { palletData } from '../../data/pallets';

import {
  PivotTableComponent,
  Aggregators,
  PaginatorComponent,
  AvatarComponent
} from '../../dist/components';

const columnMappings = [
  { heading: 'Company', property: 'company' },
  { heading: 'Pallet Number', property: 'palletNumber' },
  { heading: 'Packing Week', property: 'packingWeek' },
  { heading: 'Vessel ID', property: 'vesselId' },
  { heading: 'PI Date', property: 'piDate' },
  { heading: 'PO Date', property: 'poDate' },
  { heading: 'Container Number', property: 'containerNumber' },
  { heading: 'TM', property: 'tm' },
  { heading: 'Comm', property: 'comm' },
  { heading: 'Variety', property: 'varietyName' },
  { heading: 'Class', property: 'class' },
  { heading: 'Pack', property: 'pack' },
  { heading: 'Count', property: 'count' },
  { heading: 'State', property: 'state', sticky: true },
  { heading: 'Farm Variety', property: 'originFarmData_accessObject_Farm Variety'},
  { heading: 'Farm Code', property: 'originFarmData_accessObject_Farm Code'},
  { heading: 'Optional Note', property: 'originFarmData_accessObject_Optional Note'}
  // { heading: 'Cartons', property: 'cartons' }
];

const aggregatedColumns = [
  {
    property: 'cartons',
    aggregator: Aggregators.sum,
    heading: 'Total Cartons'
  }, {
    property: 'priceMinorUnit',
    aggregator: (iterable: number[]) => `£ ${(Aggregators.sum(iterable) * 0.05).toFixed(2)}`,
    heading: 'Total Price'
  }
];

const stickyRows = [
  {
    heading: 'Grand Totals',
    aggregator: Aggregators.sum,
    properties: ['cartons', 'priceMinorUnit']
  },
  {
    heading: 'Only Price',
    aggregator: Aggregators.sum,
    properties: ['priceMinorUnit']
  },
  {
    heading: 'Weird Property',
    aggregator: Aggregators.sum,
    properties: ['strange']
  },
];

const selectedColumns = [
  { heading: 'Company', property: 'company' },
  { heading: 'Container Number', property: 'containerNumber' },
  { heading: 'Pallet Number', property: 'palletNumber' },
  { heading: 'State', property: 'state', sticky: true },
];

storiesOf('Pivot Table', module)
  .addDecorator(
    moduleMetadata({
      declarations: [PivotTableComponent, PaginatorComponent],
      imports: [CommonModule],
    }),
  )
  .add('Pivot table with data', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [isFullWidth]="isFullWidth"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        isFullWidth: true,
        data: palletData
      },
    };
  })
  .add('Pivot table with properties selected', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [selectedColumns]="selectedColumns"
          [isFullWidth]="isFullWidth"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        selectedColumns,
        isFullWidth: true,
        data: palletData
      },
    };
  })
  .add('Pivot table with sticky rows', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [selectedColumns]="selectedColumns"
          [stickyRows]="stickyRows"
          [isFullWidth]="isFullWidth"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        selectedColumns,
        stickyRows,
        isFullWidth: true,
        data: palletData
      },
    };
  })
  .add('Pivot table with column selection disabled', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [selectedColumns]="selectedColumns"
          [isFullWidth]="isFullWidth"
          [enableColumnSelection]="false"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        selectedColumns,
        isFullWidth: true,
        data: palletData
      },
    };
  })
  .add('Static table pivoted on selection', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [selectedColumns]="selectedColumns"
          [isFullWidth]="isFullWidth"
          [staticColumns]="true"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        selectedColumns,
        isFullWidth: true,
        data: palletData
      },
    };
  })
  .add('Pivot table with custom content', () => {
    const acceptRejectTemplate = `
      <ng-template let-row #customContent>
        <div class="field has-addons" style="float: right">
          <div class="control">
            <button class="button is-default has-text-success"
                    (click)="onAccept(row)">
            Accept
            </button>
          </div>
          <div class="control">
            <button class="button is-default has-text-danger"
                    (click)="onReject(row)">
            Reject
            </button>
          </div>
        </div>
      </ng-template>
    `;
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [selectedColumns]="selectedColumns"
          [isFullWidth]="isFullWidth"
          [enableCustomContent]="enableCustomContent"
          [data]="data">

          ${acceptRejectTemplate}

        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        selectedColumns,
        isFullWidth: true,
        enableCustomContent: true,
        onAccept: (row: any[]) => {
          const ids = row.find(r => r.property === 'id').value;
          palletData.forEach(p => {
            if (ids.includes(p.id)) {
              p.state = 'accepted';
            }
          });
        },
        onReject: (row: any[]) => {
          const ids = row.find(r => r.property === 'id').value;
          palletData.forEach(p => {
            if (ids.includes(p.id)) {
              p.state = 'rejected';
            }
          });
        },
        data: palletData
      },
    };
  })
  .add('Pivot table with no data', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [isFullWidth]="isFullWidth"
          [data]="data">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        isFullWidth: true,
        data: []
      },
    };
  })
  .add('Pivot table with loading state', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <button type="button" class="button is-info" (click)="isLoading = !isLoading">
          Toggle loading state:&nbsp;<b>{{ isLoading }}</b>
        </button>
      </div>
      <div style="padding: 0 3rem 0">
        <agx-pivot-table
          [columnMappings]="columnMappings"
          [aggregatedColumns]="aggregatedColumns"
          [isFullWidth]="isFullWidth"
          [data]="data"
          [isLoading]="isLoading">
        </agx-pivot-table>
      </div>
    `,
      props: {
        columnMappings,
        aggregatedColumns,
        isFullWidth: true,
        data: palletData,
        isLoading: true
      },
    };
  });


const paginatorActions = {
  onPageChange: action('page change')
};

storiesOf('Paginator', module)
  .addDecorator(
    moduleMetadata({
      declarations: [PaginatorComponent],
      imports: [CommonModule],
    }),
  )
  .add('Default paginator', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-paginator
          [totalItems]="totalItems"
          [itemsPerPage]="itemsPerPage"
          (pageChange)="pageChange($event)">
        </agx-paginator>
      </div>
    `,
      props: {
        totalItems: 20,
        itemsPerPage: 5,
        pageChange: paginatorActions.onPageChange
      },
    };
  });

storiesOf('Avatar', module)
  .addDecorator(
    moduleMetadata({
      declarations: [AvatarComponent],
      imports: [CommonModule, BrowserAnimationsModule],
    }),
  )
  .add('Avatar', () => {
    return {
      template: `
      <div style="padding: 3rem">
        <agx-avatar [label]="label"
          [uploading]="isUploading"
          [uploadProgress]="uploadProgress"
          [avatarUrl]="avatarUrl">
        </agx-avatar>
        <div style="padding-top: 1rem">
          <div class="field is-grouped">
            <div class="control">
              <button type="button" class="button is-info" (click)="isUploading = !isUploading">
                Toggle uploading state:&nbsp;<b>{{ isUploading }}</b>
              </button>
            </div>
            <div class="control">
              <input
                class="input"
                type="number"
                min="0"
                max="100"
                [(ngModel)]="uploadProgress"/>
            </div>
            <div class="control">
              <select class="select" class="input" [(ngModel)]="avatarUrl">
                <option [ngValue]="null">None</option>
                <ng-container *ngFor="let url of urls">
                  <option [value]="url.value">
                    {{url.name}}
                  </option>
                </ng-container>
              </select>
            </div>
          </div>
        </div>
      </div>
    `,
      props: {
        label: 'John Doe',
        isUploading: false,
        uploadProgress: 0,
        avatarUrl: 'goldfish.jpg',
        urls: [
          { name: 'Goldfish', value: 'goldfish.jpg' },
          { name: 'Puppy', value: 'puppy.jpeg' }
        ]
      },
    };
  });



