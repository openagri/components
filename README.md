# OpenAgri Angular Components

[https://openagri.gitlab.io/components](https://openagri.gitlab.io/components)

[![pipeline status](https://gitlab.com/openagri/components/badges/master/pipeline.svg)](https://gitlab.com/openagri/components/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Module hot reload on changes

In order to develop in story book, you need to import components from `dist/components`. To keep this up to date when modifying components, run `ng build components --watch` in a terminal.

## Storybook server

Run `yarn run storybook` for a storybook server.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build components` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Publishing

We use a Gitlab CI/CD job to publish new versions of the package to npm. To publish a new version follow these steps (this assumes your code has been reviewed and in master already):  
 1. Checkout the latest master `git checkout master && git pull origin master`
 1. Bump the version value in `projects/components/package.json`.
 1. Tag the branch wih the version value e.g `git tag 0.1.7`
 1. Commit changes `git add . && gc -m 'version bump - 0.1.7'`
 1. Push changes `git push && git push --tags`

If you are unsure what the next version number should be, check out https://semver.org/

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
