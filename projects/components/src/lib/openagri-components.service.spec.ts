import { TestBed } from '@angular/core/testing';

import { OpenAgriComponentsService } from './openagri-components.service';

describe('OpenAgriComponentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OpenAgriComponentsService = TestBed.get(OpenAgriComponentsService);
    expect(service).toBeTruthy();
  });
});
