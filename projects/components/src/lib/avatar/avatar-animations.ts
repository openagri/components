import {
  trigger,
  animate,
  transition,
  style
} from '@angular/animations';

export const fadeInAnimation = trigger('fadeInAnimation', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('0.25s ease', style({ opacity: 1 }))
  ]),
  transition(':leave', [
    style({ opacity: 1, transform: 'translateX(0)' }),
    animate('0.25s ease', style({ opacity: 0 }))
  ])
]);

export const avatarAnimations = [
  fadeInAnimation
];
