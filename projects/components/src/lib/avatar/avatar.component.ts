import { Component, OnInit, Input, ElementRef, ViewChild, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { avatarAnimations } from './avatar-animations';

@Component({
  selector: 'agx-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
  animations: [...avatarAnimations],
})
export class AvatarComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() avatarUrl: string;
  @Input() label: string;
  @Input() width = 80;
  @Input() isCentered = false;
  @Input() uploadProgress = 0;
  @Input() uploading = false;
  @Input() loadingImage = false;

  marginStyle = '';

  @ViewChild('image', { static: true }) imageEl: ElementRef<HTMLImageElement>;
  @ViewChild('progressRingCircle', { static: true }) progressCircleEl: ElementRef<SVGElement>;

  get initial() {
    if (!this.label) {
      return null;
    }
    return this.label.charAt(0).toUpperCase();
  }

  constructor() { }

  ngOnInit() {
    this.marginStyle = this.isCentered ? '0 auto 0' : '';
  }

  ngAfterViewInit(): void {
    if (this.avatarUrl) {
      this.loadingImage = true;
      const image = this.imageEl.nativeElement;
      image.src = this.avatarUrl;
      image.onload = () => {
        this.loadingImage = false;
      };
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.avatarUrl) {
      if (changes.avatarUrl.currentValue) {
        this.loadingImage = true;
        const image = this.imageEl.nativeElement;
        image.src = changes.avatarUrl.currentValue;
        image.onload = () => {
          this.loadingImage = false;
        };
      }
    }

    if (changes.uploading && changes.uploading.currentValue && this.progressCircleEl) {
      const radius = (this.width / 2) - 2;
      const circumference = radius * 2 * Math.PI;
      this.progressCircleEl.nativeElement.style.strokeDasharray = `${circumference} ${circumference}`;
      const offset = circumference - this.uploadProgress / 100 * circumference;
      this.progressCircleEl.nativeElement.style.strokeDashoffset = offset.toString();
    }

    if (changes.uploadProgress && this.progressCircleEl) {
      const radius = (this.width / 2) - 2;
      const circumference = radius * 2 * Math.PI;
      const offset = circumference - changes.uploadProgress.currentValue / 100 * circumference;
      this.progressCircleEl.nativeElement.style.strokeDashoffset = offset.toString();
    }
  }

  get fontSize() {
    if (this.width <= 30) {
      return '1rem';
    } else if (this.width <= 40) {
      return '1.5rem';
    } else {
      return '2.5rem';
    }
  }

  get iconSize() {
    if (this.width <= 30) {
      return 'md';
    } else if (this.width <= 40) {
      return 'lg';
    } else {
      return 'lg';
    }
  }

}
