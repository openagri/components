import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PivotTableComponent } from './pivot-table/pivot-table.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { AvatarComponent } from './avatar/avatar.component';

@NgModule({
  declarations: [PivotTableComponent, PaginatorComponent, AvatarComponent],
  imports: [
    CommonModule
  ],
  exports: [PivotTableComponent, PaginatorComponent, AvatarComponent]
})
export class OpenAgriComponentsModule { }
