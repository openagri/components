// @dynamic
export class Aggregators {
  static sum(numbers: Array<number>): number {
    return numbers.reduce((prev, curr) => prev + curr, 0);
  }

  static count(iterable: Array<number | string>): number {
    return new Set(iterable).size;
  }
}
