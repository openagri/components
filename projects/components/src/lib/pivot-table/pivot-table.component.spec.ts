import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PivotTableComponent } from './pivot-table.component';
import { Aggregators } from './aggregators';
import { DebugElement, SimpleChange } from '@angular/core';
import { PaginatorComponent } from '../paginator/paginator.component';

describe('PivotTableComponent', () => {
  let component: PivotTableComponent;
  let fixture: ComponentFixture<PivotTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PivotTableComponent, PaginatorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PivotTableComponent);
    component = fixture.componentInstance;

    component.columnMappings = [{
      heading: 'Property 1',
      property: 'property1'
    }, {
      heading: 'Property 2',
      property: 'property2'
    }, {
      heading: 'Property 3',
      property: 'property3'
    }];

    component.data = [
      { property1: 'Mock 1 property1', property2: 'A', property3: 10 },
      { property1: 'Mock 2 property1', property2: 'A', property3: 10 },
      { property1: 'Mock 3 property1', property2: 'A', property3: 10 }
    ];

    component.aggregatedColumns = [
      { heading: 'Total Property 3', property: 'property3', aggregator: Aggregators.sum }
    ];

    fixture.detectChanges();
  });

  // Class tests
  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.selectedColumns).toBeDefined();
    expect(component.selectedColumns.length).toBe(0);
  });

  it('should add columns and remove columns', () => {
    component.addSelectedColumn('property2');
    expect(component.selectedColumns.length).toBe(1);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[1]);

    component.removeSelectedColumn(component.columnMappings[1]);
    expect(component.selectedColumns.length).toBe(0);
  });

  it('should change columns', () => {
    component.addSelectedColumn('property2');
    expect(component.selectedColumns.length).toBe(1);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[1]);

    component.onSelectedColumnChange(0, 'property3');
    expect(component.selectedColumns.length).toBe(1);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[2]);
  });

  it('should shift columns left and right', () => {
    component.addSelectedColumn('property1');
    component.addSelectedColumn('property2');
    expect(component.selectedColumns.length).toBe(2);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[0]);
    expect(component.selectedColumns[1]).toEqual(component.columnMappings[1]);

    component.moveColumnRight(0);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[1]);
    expect(component.selectedColumns[1]).toEqual(component.columnMappings[0]);

    component.moveColumnLeft(1);
    expect(component.selectedColumns[0]).toEqual(component.columnMappings[0]);
    expect(component.selectedColumns[1]).toEqual(component.columnMappings[1]);
  });

  // DOM Tests
  it('should render headings for each selected row', () => {
    const debugElement: DebugElement = fixture.debugElement;
    let headingElement = debugElement.query(By.css('th'));
    let th: HTMLElement = headingElement.nativeElement;

    expect(th.textContent).toContain('Total Property 3');
    component.addSelectedColumn('property1');
    component.addSelectedColumn('property2');

    fixture.detectChanges();

    headingElement = debugElement.query(By.css('th'));
    th = headingElement.nativeElement;
    expect(th.textContent).toContain('Property 1');

    component.moveColumnRight(0);
    fixture.detectChanges();

    headingElement = debugElement.query(By.css('th'));
    th = headingElement.nativeElement;
    expect(th.textContent).toContain('Property 2');
  });

  it('should re-render the table when the data has changed', () => {
    spyOn(component, 'buildViewModel');
    const data = [
      { property1: 'Mock 1 property1', property2: 'A', property3: 10 },
      { property1: 'Mock 2 property1', property2: 'A', property3: 10 }
    ];

    // act
    component.data = data;

    component.ngOnChanges({
      data: new SimpleChange(null, data, false)
    });

    fixture.detectChanges();

    // assert
    expect(component.buildViewModel).toHaveBeenCalled();
    expect(component.data).toEqual(data);
  });

});
