import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  OnChanges,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';

export interface ColumnMapping {
  property: string;
  heading: string;
  sticky?: boolean;
}

export interface AggregatedColumn {
  property: string;
  aggregator: (iterable: any[]) => any;
  heading: string;
  hidden?: boolean;
}

export interface StickyRow {
  properties: string[];
  values?: any[];
  aggregator: (iterable: any[]) => string | number;
  heading: string;
}

interface Column {
  property: string;
  value: string | number;
  rowspan: number;
}

const isObject = (object: object) => {
  return typeof object === 'object' && !Array.isArray(object);
};

@Component({
  selector: 'agx-pivot-table',
  templateUrl: './pivot-table.component.html',
  styleUrls: ['./pivot-table.component.scss']
})
export class PivotTableComponent implements OnInit, OnChanges {

  constructor() { }

  @ContentChild('customContent', {static: false}) template: TemplateRef<any>;

  @Input()
  data: any[] = [];

  @Input()
  columnMappings: ColumnMapping[];

  @Input()
  aggregatedColumns: AggregatedColumn[] = [];

  @Input()
  stickyRows: StickyRow[] = [];

  @Input()
  isFullWidth = false;

  @Input()
  itemsPerPage = 20;

  @Input()
  isLoading = false;

  @Input()
  enableColumnSelection = true;

  @Input()
  enableCustomContent = false;

  @Input()
  staticColumns = false;

  // tslint:disable-next-line: variable-name
  _selectedColumns: ColumnMapping[] = [];

  @Input()
  get selectedColumns(): ColumnMapping[] {
    return this._selectedColumns;
  }

  set selectedColumns(selectedColumns: ColumnMapping[]) {
    if (selectedColumns === this._selectedColumns) {
      return;
    }
    this._selectedColumns = selectedColumns;
    this.selectedColumnsChange.emit(this._selectedColumns);
  }

  @Output()
  selectedColumnsChange = new EventEmitter<ColumnMapping[]>();

  currentPage = 1;

  rows: Column[][] = [];

  groupedData: any = [];

  get totalItems() {
    return this.rows.length;
  }

  get pageLowerBound() {
    return (this.currentPage * this.itemsPerPage) - this.itemsPerPage;
  }

  get pageUpperBound() {
    return this.currentPage * this.itemsPerPage;
  }

  get pageRows() {
    const pageRows = this.rows.slice(this.pageLowerBound, this.pageUpperBound);
    this.setColumnRowSpans(this.selectedColumns, pageRows);
    return pageRows;
  }

  ngOnInit() {
    if (this.staticColumns) {
      this.enableColumnSelection = false;
    }
    this.aggregatedColumns.push({
      property: 'id',
      aggregator: (iterable: number[]) => iterable,
      heading: 'ID',
      hidden: true
    });
    this.rows = [];
    this.rows.push(this.aggregatedColumns.map((ac) => {
      const data = this.data.map(obj => obj[ac.property]);
      return {
        property: ac.property,
        value: ac.aggregator(data),
        rowspan: 1,
        hidden: ac.hidden
      };
    }));
    this.buildViewModel();
    this.buildStickyRows();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data || changes.selectedColumns) {
      this.buildViewModel();
    }
  }

  /**
   * Fired when the new column dropdown change event is fired,
   * adds to selected columns and rebuilds table rows
   * @param property The property to add to the selected columns
   */
  addSelectedColumn(property: string) {
    let mapping;
    if (property === 'id') {
      const index = this.aggregatedColumns.indexOf(this.aggregatedColumns.find((ac) => ac.property === 'id'));
      if (index !== -1) { this.aggregatedColumns.splice(index, 1); }
      mapping = {
        property: 'id',
        heading: 'ID',
        sticky: false
      };
    } else {
      mapping = this.columnMappings.find((m) => m.property === property);
    }
    if (!this.isColumnSelected(mapping)) {
      this.selectedColumns.push(mapping);
      this.buildViewModel();
    }
  }

  /**
   * Removes a selected column and rebuilds the table rows
   * @param mapping The ColumnMapping to remove from the selected rows
   */
  removeSelectedColumn(mapping: ColumnMapping) {
    if (mapping.property === 'id') {
      this.aggregatedColumns.push({
        property: 'id',
        aggregator: (iterable: number[]) => iterable,
        heading: 'ID',
        hidden: true
      });
    }
    this.selectedColumns.splice(this.selectedColumns.indexOf(mapping), 1);
    this.buildViewModel();
  }

  /**
   * Change an existing selected column to a new column
   */
  onSelectedColumnChange(index: number, property: string) {
    const mapping = this.columnMappings.find(m => m.property === property);
    if (!this.isColumnSelected(mapping)) {
      this.selectedColumns[index] =  mapping;
      this.buildViewModel();
    }
  }

  /**
   * Move a specified column one index to the right
   * @param index The index of the column to shift
   */
  moveColumnRight(index: number) {
    const temp = this.selectedColumns[index];
    this.selectedColumns[index] = this.selectedColumns[index + 1];
    this.selectedColumns[index + 1] = temp;
    this.buildViewModel();
  }

  /**
   * Move a specified column one index to the left
   * @param index The index of the column to shift
   */
  moveColumnLeft(index: number) {
    const temp = this.selectedColumns[index];
    this.selectedColumns[index] = this.selectedColumns[index - 1];
    this.selectedColumns[index - 1] = temp;
    this.buildViewModel();
  }

  isColumnSelected(columnMapping: ColumnMapping): boolean {
    return this.selectedColumns.findIndex(selected => selected.property === columnMapping.property) > -1;
  }

  onPageChange(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  /**
   * Clear the column selections
   */
  clearSelections() {
    this.selectedColumns = [];
    this.buildViewModel();
  }

  /**
   * Build the table rows
   */
  buildViewModel() {
    if (this.selectedColumns.length > 0) {
      this.groupedData = this.nest(this.selectedColumns.map((c) => c.property), this.data);
      this.rows = [];
      this.traverseGroupedData(this.groupedData);
    } else {
      this.groupedData = [];
      this.rows = [];
      this.rows.push(this.aggregatedColumns.map((ac) => {
        const data = this.data.map(obj => obj[ac.property]);
        return { property: ac.property, value: ac.aggregator(data), rowspan: 1, hidden: ac.hidden };
      }));
    }
  }

  /**
   * Recursively grouped data into a nested object based on property keys
   * @param keys The keys to group the data on
   * @param array The data to group
   */
  private nest(keys: string[], array: object[]) {
    if (keys.length === 0) {
      return array;
    }

    const first = keys[0];
    const rest = keys.slice(1);

    return this.mapValue(this.groupBy(first, array), (value: object[]) => {
      return this.nest(rest, value);
    });
  }

  /**
   * Get a copy of an object after a mutator has been applied to each property
   * @param object The object to copy
   * @param iteratee The property mutator
   */
  private mapValue(object: object, iteratee: any) {
    object = Object(object);
    const result = {};
    Object.keys(object).forEach((key) => {
      result[key] = iteratee(object[key]);
    });
    return result;
  }

  /**
   * Groups an array of objects based on a object key
   * @param key The object key to group the data on
   * @param array The array to group
   */
  private groupBy(key: string, array: object[]) {
    const objectsByKeyValue = {};
    // simple for loop for best performance
    for (let i = 0, l = array.length; i < l; i++) {
      let value = array[i][key];
      if (!value && key.includes('_accessObject_')) {
        // dig down into specified object(s)
        value = key.split('_accessObject_').reduce((j, k) => j && j[k], array[i]);
      }
      if (typeof objectsByKeyValue[value] === 'undefined') {
        objectsByKeyValue[value] = [];
      }
      objectsByKeyValue[value].push(array[i]);
    }
    return objectsByKeyValue;
  }

  /**
   * Recursively traverse the grouped data and build table rows
   * @param data The grouped data object
   */
  private traverseGroupedData(data: object) {
    if (isObject(data)) {
      const keys = Object.keys(data);
      keys.forEach(key => {
        if (isObject(data[key])) {
          this.traverseGroupedData(data[key]);
        } else {
          this.rows.push(this.selectedColumns.map((column) => {
            let value = data[key][0][column.property];
            if (!value && column.property.includes('_accessObject_')) {
              // If no value and property contains specific string indicating we want to dig down
              // into a nested object, split on the string and traverse the object.
              // Arbitrary levels of nesting is supported
              value = column.property.split('_accessObject_').reduce((j, k) => j && j[k], data[key][0]);
            }
            return {
              property: column.property,
              value,
              rowspan: 1
            };
          }).concat(this.aggregatedColumns.map((ac) => {
            const groupedData = data[key].map((obj: object) => obj[ac.property]);
            return {
              property: ac.property,
              value: ac.aggregator(groupedData),
              rowspan: 1,
              hidden: ac.hidden
            };
          })));
        }
      });
    }
  }

  /**
   * Recursivley set the column rowspans of a table based upon the occourence of value in the data subset
   */
  private setColumnRowSpans(selectedColumns: ColumnMapping[], rows: Column[][]) {
    if (selectedColumns.length > 0 && rows.length > 0) {
      const columnMapping = selectedColumns[0];
      const groupedRows = [];
      const processedValues = [];

      if (rows.length > 0) {
        rows.forEach((row) => {
          const column = row.find(c => c.property === columnMapping.property);
          if (column) {
            if (processedValues.indexOf(column.value) === -1) {
              processedValues.push(column.value);
              groupedRows.push([row]);
            } else {
              groupedRows[groupedRows.length - 1].push(row);
              const first = groupedRows[groupedRows.length - 1][0].find((c: Column) => c.property === column.property);
              first.rowspan++;
              row.splice(row.indexOf(column), 1);
            }
          }
        });
      }

      selectedColumns = selectedColumns.slice(1);
      groupedRows.forEach(subset => this.setColumnRowSpans(selectedColumns, subset));
    }
  }

  buildStickyRows() {
    if (this.stickyRows) {
      // Reset sticky row values
      this.stickyRows.forEach(stickyRow => {
        stickyRow.values = Array(this.aggregatedColumns.filter(ac => !ac.hidden).length).fill('-');
      });

      // Loop though aggregated columns first so that we calculate
      // sticky row values in order of ACs.
      this.aggregatedColumns.forEach((ac, index) => {
        this.stickyRows.forEach(stickyRow => {
          stickyRow.properties.forEach(property => {
            if (property === ac.property) {
              const data = this.data.map(obj => obj[ac.property]);
              // replace '-' with calculated value
              stickyRow.values.splice(index, 1, stickyRow.aggregator(data));
            }
          });
        });
      });
    }
  }
}
